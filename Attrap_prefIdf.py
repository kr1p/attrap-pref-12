import os
import datetime

from bs4 import BeautifulSoup
from urllib.parse import unquote

from Attrap import Attrap


class Attrap_prefIdf(Attrap):

    # Config
    __HOST = 'https://www.prefectures-regions.gouv.fr'
    __RAA_PAGE = {
        '2024': f'{__HOST}/ile-de-france/ile-de-france/ile-de-france/Documents-publications/Recueil-des-actes-administratifs/RAA-de-la-region-Ile-de-France-2024',
        '2023': f'{__HOST}/ile-de-france/ile-de-france/ile-de-france/Documents-publications/Recueil-des-actes-administratifs/RAA-de-la-region-Ile-de-France-2023',
        '2022': f'{__HOST}/ile-de-france/ile-de-france/ile-de-france/Documents-publications/Recueil-des-actes-administratifs/RAA-de-la-region-Ile-de-France-2022',
        '2021': f'{__HOST}/ile-de-france/ile-de-france/ile-de-france/Documents-publications/Recueil-des-actes-administratifs/RAA-de-la-region-Ile-de-France-2021',
        '2020': f'{__HOST}/ile-de-france/ile-de-france/ile-de-france/Documents-publications/Recueil-des-actes-administratifs/RAA-de-la-region-Ile-de-France-2020',
        '2019': f'{__HOST}/ile-de-france/ile-de-france/ile-de-france/Documents-publications/Recueil-des-actes-administratifs/RAA-de-la-region-Ile-de-France-2019',
        '2018': f'{__HOST}/ile-de-france/ile-de-france/ile-de-france/Documents-publications/Recueil-des-actes-administratifs/RAA-de-la-region-Ile-de-France-2018'
    }
    __USER_AGENT = 'Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/115.0'
    full_name = 'Préfecture d\'Île-de-France'
    short_code = 'prefIdf'

    def __init__(self, data_dir):
        super().__init__(data_dir, self.__USER_AGENT)
        self.enable_tor(10)

    def get_raa(self, keywords):
        year_pages_to_parse = []

        # Les RAA de l'Île-de-France sont éparpillés sur des sous-pages par mois.
        # Donc on parse la page principale à la recherche des sous-pages.
        if self.not_before.year <= 2024:
            year_pages_to_parse.append(self.__RAA_PAGE['2024'])
        if self.not_before.year <= 2023:
            year_pages_to_parse.append(self.__RAA_PAGE['2023'])
        if self.not_before.year <= 2022:
            year_pages_to_parse.append(self.__RAA_PAGE['2022'])
        if self.not_before.year <= 2021:
            year_pages_to_parse.append(self.__RAA_PAGE['2021'])
        if self.not_before.year <= 2020:
            year_pages_to_parse.append(self.__RAA_PAGE['2020'])
        if self.not_before.year <= 2019:
            year_pages_to_parse.append(self.__RAA_PAGE['2019'])

        pages_to_parse = []
        for year_page in year_pages_to_parse:
            page_content = self.get_page(year_page, 'get').content
            year = BeautifulSoup(page_content, 'html.parser').select('div.breadcrumb div.container p span.active')[0].get_text().split('-')[-1].strip()
            month_pages = self.get_sub_pages(
                page_content,
                'div.sommaire-bloc div.sommaire-content ol li a',
                self.__HOST,
                False
            )[::-1]
            for month_page in month_pages:
                month_date = Attrap.guess_date(f"{month_page['name']} {year}", "(.*)").replace(day=1)
                if month_date >= self.not_before.replace(day=1):
                    pages_to_parse.append(month_page['url'])

        elements = []
        for page in pages_to_parse:
            page_content = self.get_page(page, 'get').content
            for element in self.get_raa_elements(page_content):
                elements.append(element)

        self.parse_raa(elements, keywords)
        self.mailer()

    def get_raa_elements(self, page_content):
        elements = []
        # On charge le parser
        soup = BeautifulSoup(page_content, 'html.parser')

        # Pour chaque balise a, on regarde si c'est un PDF, et si oui on le parse
        for a in soup.select('main div.container.main-container div.col-main article.article div.texte div a.link-download'):
            if a.get('href') and a['href'].endswith('.pdf'):
                if a['href'].startswith('/'):
                    url = f"{self.__HOST}{a['href']}"
                else:
                    url = a['href']
                url = unquote(url)
                name = a.find('span').get_text().strip()
                # On devine la date du RAA à partir du nom de fichier
                guessed = Attrap.guess_date(name, '((?:[0-9]{2}(?:-|\\.)[0-9]{2}(?:-|\\.)20[0-9]{2})|(?:20[0-9]{2}(?:-|\\.)[0-9]{2}(?:-|\\.)[0-9]{2})\\D*^)')
                if (guessed == datetime.datetime(9999, 1, 1, 0, 0)):
                    date = None
                else:
                    date = guessed

                raa = Attrap.RAA(url, date, name)
                elements.append(raa)
        return elements
