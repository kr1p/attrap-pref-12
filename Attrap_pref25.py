import os
import datetime
import logging

from bs4 import BeautifulSoup
from urllib.parse import unquote

from Attrap import Attrap

logger = logging.getLogger(__name__)


class Attrap_pref25(Attrap):

    # Config
    __HOST = 'https://www.doubs.gouv.fr'
    __RAA_PAGE = f'{__HOST}/Publications/Publications-Legales/Recueil-des-Actes-Administratifs-RAA'

    __USER_AGENT = 'Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/115.0'

    full_name = 'Préfecture du Doubs'
    short_code = 'pref25'

    def __init__(self, data_dir):
        super().__init__(data_dir, self.__USER_AGENT)
        self.set_sleep_time(30)

    def get_raa(self, keywords):
        sub_pages = self.get_sub_pages_with_pager(
            self.__RAA_PAGE,
            'a.fr-card__link',
            'a.fr-pagination__link.fr-pagination__link--next.fr-pagination__link--lg-label',
            None,
            self.__HOST,
        )

        pages_to_parse = []

        # TODO : détecter la date de la page à partir du parsing de la page principale et non à partir de son URL
        for sub_page in sub_pages:
            url = sub_page['url']
            last_word = url.split('-')[-1]
            year = 0
            try:
                year = int(last_word)

                if self.not_before.year <= year:
                    pages_to_parse.append(url)
            except Exception as e:
                logger.warning(f"Impossible de déterminer l'année de l'URL {url}")

        elements = []
        for raa_page in pages_to_parse:
            page_content = self.get_page(raa_page, 'get').content
            elements.extend(self.get_raa_elements(page_content))

        self.parse_raa(elements[::-1], keywords)
        self.mailer()

    def get_raa_elements(self, page_content):
        # On charge le parser
        soup = BeautifulSoup(page_content, 'html.parser')

        elements = []
        # On récupère chaque balise a
        for a in soup.select('div.fr-downloads-group.fr-downloads-group--bordered ul li a'):
            if a.get('href') and a['href'].endswith('.pdf'):
                if a['href'].startswith('/'):
                    url = f"{self.__HOST}{a['href']}"
                else:
                    url = a['href']

                url = unquote(url)
                name = a.find('span').previous_sibling.replace('Télécharger ', '').strip()
                date = datetime.datetime.strptime(a.find('span').text.split(' - ')[-1].strip(), '%d/%m/%Y')

                raa = Attrap.RAA(url, date, name)
                elements.append(raa)

        return elements
