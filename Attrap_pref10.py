import os
import datetime

from bs4 import BeautifulSoup
from urllib.parse import unquote

from Attrap import Attrap


class Attrap_pref10(Attrap):

    # Config
    __HOST = 'https://www.aube.gouv.fr'
    __RAA_ARCHIVES_PAGE = f'{__HOST}/Publications/RAA-Recueil-des-Actes-Administratifs/RAA-Archives/'
    __USER_AGENT = 'Mozilla/5.0 (X11; Linux x86_64; rv:129.0) Gecko/20100101 Firefox/129.0'
    full_name = 'Préfecture de l\'Aube'
    short_code = 'pref10'

    def __init__(self, data_dir):
        super().__init__(data_dir, self.__USER_AGENT)
        self.set_sleep_time(30)

    def get_raa(self, keywords):
        # La préfecture de l'Aube a une page avec ses archives, et une page avec l'année en cours. On parse
        # donc le menu déroulant de la gauche de la page d'archives (et non le contenu de la page) pour récupérér,
        # premièrement, la page d'année en cours et, deuxièmement, les années archivées.

        pages_to_parse = []

        archives_page_content = self.get_page(self.__RAA_ARCHIVES_PAGE, 'get').content
        for year_page in self.get_sub_pages(
            archives_page_content,
            'ul.fr-sidemenu__list a.fr-sidemenu__link',
            self.__HOST,
            False,
        ):
            if Attrap.guess_date(year_page['name'], '.* ([0-9]{4})').year >= self.not_before.year:
                pages_to_parse.append(year_page['url'])

        elements = self.get_raa_with_pager(
            pages_to_parse,
            'ul.fr-pagination__list li a.fr-pagination__link.fr-pagination__link--next',
            self.__HOST
        )

        self.parse_raa(elements, keywords)
        self.mailer()

    def get_raa_elements(self, page_content):
        elements = []
        # On charge le parser
        soup = BeautifulSoup(page_content, 'html.parser')

        # On récupère chaque balise a
        for a in soup.select('a.fr-card__link.menu-item-link'):
            if a.get('href') and a['href'].endswith('.pdf'):
                if a['href'].startswith('/'):
                    url = f"{self.__HOST}{a['href']}"
                else:
                    url = a['href']

                url = unquote(url)
                name = a.text.strip()
                date = datetime.datetime.strptime(a['title'].split(' - ')[-1].strip(), '%d/%m/%Y')

                if date >= self.not_before:
                    raa = Attrap.RAA(url, date, name)
                    elements.append(raa)

        return elements
